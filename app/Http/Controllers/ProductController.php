<?php

namespace App\Http\Controllers;

use App\Http\Traits\ShedTrait;
use App\Http\Traits\TimberTrait;
use App\Http\Traits\HouseholdTrait;
use App\Http\Traits\GardenTrait;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    use TimberTrait;
    use ShedTrait;
    use HouseholdTrait;
    use GardenTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    //view products
    public function viewProductListForCategory($id, $action)
    {
        return view('products.productList', [
            'products' => $this->getProductInformationForCategory($id, $action),
            'category' => $id
        ]);
    }

    //edit product
    public function editProduct($id, $action)
    {
        $product    = Product::where('id', $id)->first();
        $categories = Category::whereIn('id', [1, 2, 3, 4])->get();

        return view('products.productEdit', [
            'product'    => $product,
            'categories' => $categories
        ]);
    }

    //update product
    public function updateProduct(Request $request, $id)
    {
        $product  = Product::where('id', $id)->first();
        $category = Category::where('id', $product->category)->first();

        if ($request->image) {
            $image = $this->uploadProductImage($request);
        } else {
            $image = $product->image;
        }

        $product->update([
            'category'         => $category->id,
            'name'             => $request->name,
            'description'      => $request->description,
            'image'            => $image,
            'size'             => $request->size,
            'price'            => $request->price,
            'sale'             => isset($request->sale) ? 1 : 0,
            'sale_price'       => $request->sale_price,
            'marketplace_link' => $request->marketplace_link
        ]);

        return redirect('/' . $category->name)->with('success', 'Product updated successfully!');
    }

    //add new product
    public function viewNewProductPage()
    {
        $categories = Category::whereIn('id', [1, 2, 3, 4])->get();

        return view('products.productNew', [
            'categories' => $categories
        ]);
    }

    //store product to database
    public function storeNewProduct(Request $request)
    {
        $image = '';

        if ($request->image) {
            $image = $this->uploadProductImage($request);
        }

        $category = Category::where('id', $request->category)->first();

        Product::create([
            'category'         => $category->id,
            'name'             => $request->name,
            'description'      => $request->description,
            'image'            => $image,
            'size'             => $request->size,
            'price'            => $request->price,
            'sale'             => isset($request->sale) ? 1 : 0,
            'sale_price'       => $request->sale_price,
            'marketplace_link' => $request->marketplace_link
        ]);

        return redirect('/' . $category->name)->with('success', 'New product added successfully!');
    }

    //delete product from database
    public function deleteProduct(Request $request, $id)
    {
        $product  = Product::where('id', $id)->first();
        $category = Category::where('id', $product->category)->first()->name;

        File::delete(public_path('/images/products/') . $product->image);

        $product->delete();

        return redirect('/' . $category)->with('success', 'Product deleted successfully!');
    }

    //get product information
    public function getProductInformationForCategory($id, $action)
    {
        return Product::join('categories', 'categories.id', '=', 'products.category')
            ->when($action == 'view', function ($query) use ($id) {
                return $query->where('categories.id', $id);
            })
            ->when($action == 'edit', function ($query) use ($id) {
                return $query->where('products.id', $id);
            })
            ->get([
                'categories.name as category',
                'products.id',
                'products.name',
                'products.description',
                'products.image',
                'products.size',
                'products.price',
                'products.sale',
                'products.sale_price',
                'products.created_at',
            ]);
    }

    public function uploadProductImage($request)
    {
        if ($request->image) {

            $fileName = $request->image->getClientOriginalName();

            $request->image->move(public_path('/images/products'), $fileName);

        } else {

            $fileName = '';
        }

        return $fileName;
    }
}
