<?php

namespace App\Http\Controllers;

use App\Models\Option;
use App\Models\Page;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function settingsPage()
    {
        return view('settingsPage', [
            'contactInfo' => $this->contactTab(),
            'home'        => $this->homeInfo(),
            'addressInfo' => $this->addressTab(),
            'hours'       => $this->hoursTab(),
            'socials'     => $this->socialTab(),
            'about'       => $this->aboutTab(),
        ]);
    }

    public function homeInfo()
    {
        return Page::where('name', 'Home')->first();
    }

    public function contactTab()
    {
        return Option::whereIn('type', [1, 2])->where('display', 1)->get();
    }

    public function addressTab()
    {
        return Option::where('type', 3)->get();
    }

    public function hoursTab()
    {
        return Option::where('type', 5)->get();
    }

    public function socialTab()
    {
        return Option::where('type', 4)->get();
    }

    public function aboutTab()
    {
        return Page::where('name', 'About')->first();
    }

    public function updateSettings(Request $request)
    {
        $settings = $request->all();

        foreach ($settings as $key => $setting) {

            if ($key != '_token') {
                if ($key != 'image' || $key != 'description' || $key != 'home_image' || $key != 'home_h1' || $key != 'home_h2') {

                    $display = 0;

                    if ($setting) {
                        $display = 1;
                    }

                    Option::where('value', $key)->update([
                        'description' => $setting,
                        'display'     => $display
                    ]);
                }

                if ($key == 'home_image') {

                    if ($setting) {
                        $homeImage = $this->uploadImage($request);
                    } else {
                        $homeImage = $setting->home_image;
                    }

                    Page::where('id', 1)->update([
                        'image' => $homeImage
                    ]);
                }

                if ($key == 'home_h1') {

                    Page::where('id', 1)->update([
                        'h1' => $setting
                    ]);
                }

                if ($key == 'home_h2') {

                    Page::where('id', 1)->update([
                        'h2' => $setting
                    ]);
                }

                if ($key == 'description') {
                    Page::where('id', 2)->update([
                        'description' => $setting
                    ]);
                }

                if ($key == 'image') {

                    if ($setting) {
                        $image = $this->uploadImage($request);
                    } else {
                        $image = $setting->image;
                    }

                    Page::where('id', 2)->update([
                        'image' => $image
                    ]);
                }
            }

        }

        return redirect()->back()->with('success', 'Settings have been updated');
    }

    public function uploadImage($request)
    {
        if ($request->image) {

            $fileName = $request->image->getClientOriginalName();

            if (!File::exists(public_path(public_path('/images/settings'), $fileName))) {
                $request->image->move(public_path('/images/settings'), $fileName);
            }

            return $fileName;
        }

        if ($request->home_image) {

            $fileName = $request->home_image->getClientOriginalName();

            if (!File::exists(public_path(public_path('/images/settings'), $fileName))) {
                $request->home_image->move(public_path('/images/settings'), $fileName);
            }

            return $fileName;
        }
    }
}
