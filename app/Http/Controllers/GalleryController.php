<?php

namespace App\Http\Controllers;

use App\Models\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class GalleryController extends Controller
{
    public function galleryPage()
    {
        return view('galleryPage', [
            'images' => Gallery::orderBy('updated_at', 'desc')->get()
        ]);
    }

    public function storeNewImage(Request $request)
    {
        $image = '';

        if ($request->image) {
            $image = $this->uploadImage($request);
        }

        Gallery::create([
            'name'  => $request->name,
            'image' => $image
        ]);

        return redirect()->back()->with('success', 'New Image added');
    }

    public function updateImage(Request $request, $id)
    {
        $GalleryImage = Gallery::where('id', $id)->first();

        if ($request->image) {
            $image = $this->uploadImage($request);
        } else {
            $image = $GalleryImage->image;
        }

        $GalleryImage->update([
            'name'             => $request->name,
            'image'            => $image,
        ]);

        return redirect()->back()->with('success', 'Image has been updated');
    }

    public function deleteImage(Request $request, $id)
    {
        $GalleryImage  = Gallery::where('id', $id)->first();

        File::delete(public_path('/images/gallery/') . $GalleryImage->image);

        $GalleryImage->delete();

        return redirect()->back()->with('success', 'Image has been deleted');
    }

    public function uploadImage($request)
    {
        if ($request->image) {

            $fileName = $request->image->getClientOriginalName();

            $request->image->move(public_path('/images/gallery'), $fileName);

        } else {

            $fileName = '';
        }

        return $fileName;
    }
}
