<?php

namespace App\Http\Traits;

trait TimberTrait
{
    public function timberList($id = 1)
    {
        return $this->viewProductListForCategory($id, $action = 'view');
    }

    public function timberEdit($id)
    {
        return $this->editProduct($id, $action = 'edit');
    }
}
