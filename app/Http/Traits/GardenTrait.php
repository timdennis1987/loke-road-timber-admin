<?php

namespace App\Http\Traits;

trait GardenTrait
{
    public function gardenList($id = 4)
    {
        return $this->viewProductListForCategory($id, $action = 'view');
    }

    public function gardenEdit($id)
    {
        return $this->editProduct($id, $action = 'edit');
    }
}
