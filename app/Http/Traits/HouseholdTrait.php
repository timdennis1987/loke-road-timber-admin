<?php

namespace App\Http\Traits;

trait HouseholdTrait
{
    public function householdList($id = 3)
    {
        return $this->viewProductListForCategory($id, $action = 'view');
    }

    public function householdEdit($id)
    {
        return $this->editProduct($id, $action = 'edit');
    }
}
