<?php

namespace App\Http\Traits;

trait ShedTrait
{
    public function shedList($id = 2)
    {
        return $this->viewProductListForCategory($id, $action = 'view');
    }

    public function shedEdit($id)
    {
        return $this->editProduct($id, $action = 'edit');
    }
}
