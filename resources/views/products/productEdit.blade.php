@extends('layouts.app')
@section('content')

    <form action="/product/{{ $product->id }}/update" method="post" enctype="multipart/form-data">
        @csrf

        <button type="submit" class="btn btn-success">Update Product</button>
        <a href="/product/delete/{{ $product->id }}" class="btn btn-danger float-right del">Delete</a>

        <div class="row mt-3">

            <div class="col-md-6 my-2">
                <input type="text"
                       class="form-control"
                       id="name"
                       name="name"
                       value="{{ $product->name }}"
                       placeholder="Title of Product">
            </div>

            <div class="col-md-2 my-2">
                <select class="form-control"
                        id="category"
                        name="category">
                    <option selected>- Category -</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ $product->category == $category->id ? 'selected' : '' }}>
                            {{ ucwords($category->name) }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-2 my-2">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">£</div>
                    </div>
                    <input type="text"
                           class="form-control"
                           id="price"
                           name="price"
                           value="{{ $product->price }}"
                           placeholder="0.00">
                </div>
            </div>

            <div class="col-md-2 my-2">
                <div class="input-group mb-2">
                    <input type="text"
                           class="form-control"
                           id="size"
                           name="size"
                           value="{{ $product->size }}"
                           placeholder="Size">
                </div>
            </div>

            <div class="col-12 my-2">
                <div class="form-group">
                    <textarea class="form-control"
                              id="description"
                              rows="5"
                              name="description"
                              placeholder="Description of Product">{{ $product->description }}</textarea>
                </div>
            </div>

            <div class="col-12 my-2">
                <div class="custom-file">
                    <input type="file"
                           class="custom-file-input"
                           id="image"
                           name="image"
                           value="{{ $product->image }}">
                    <label class="custom-file-label"
                           for="image">{{ $product->image ? strlen($product->image) > 20 ? substr($product->image,0,20)."..." : $product->image : 'Browse Image' }}</label>
                    <small class="form-text text-muted">
                        Image of Product
                    </small>
                </div>
            </div>

            <div class="col-lg-2 col-md-4 my-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="sale" value="1"
                           name="sale" {{ $product->sale == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="sale">On Sale?</label>
                </div>
            </div>

            <div class="col-lg-10 col-md-8 my-2">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">£</div>
                    </div>
                    <input type="text"
                           class="form-control"
                           id="sale_price"
                           name="sale_price"
                           value="{{ $product->sale_price }}"
                           placeholder="Sale Price">
                </div>
            </div>

            <div class="col my-2">
                <label for="marketplace_link">Marketplace Link</label>
                <input type="text"
                       class="form-control"
                       id="marketplace_link"
                       name="marketplace_link"
                       value="{{ $product->marketplace_link }}"
                       placeholder="Paste your Facebook Marketplace link here...">
            </div>

        </div>
    </form>

@endsection
