@extends('layouts.app')
@section('content')

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ session('success') }}
        </div>
    @endif

    <a class="btn btn-primary my-3" href="product/new?category={{ $category }}">Add new product</a>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col" class="text-center">Image</th>
            <th scope="col" class="text-center">Price</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <td>
                    <a href="/{{ $product->category }}/edit/{{ $product->id }}">
                        {{ strlen($product->name) > 10 ? substr($product->name,0,10)."..." : $product->name }}
                    </a>
                </td>
                <td class="text-center">
                    <img width="100"
                         src="public/images/products/{{ $product->image }}"
                         alt="">
                </td>
                <td class="text-center">
                    £
                    @if($product->sale)
                        <span class="text-danger">{{ $product->sale_price }}</span>
                    @else
                        <span>{{ $product->price }}</span>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
