@extends('layouts.app')
@section('content')

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    @endif

    <form action="/product/new/store" method="post" enctype="multipart/form-data">
        @csrf

        <button type="submit" class="btn btn-success">Save Product</button>

        <div class="row mt-3">

            <div class="col-md-6 my-2">
                <input type="text"
                       class="form-control"
                       id="name"
                       name="name"
                       placeholder="Title of Product" required>
            </div>

            <div class="col-md-2 my-2">
                <select class="form-control"
                        name="category"
                        id="category">
                    <option selected>- Category -</option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ $_GET['category'] == $category->id ? 'selected' : '' }}>
                            {{ ucwords($category->name) }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-2 my-2">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">£</div>
                    </div>
                    <input type="text"
                           class="form-control"
                           id="price"
                           name="price"
                           placeholder="0.00" required>
                </div>
            </div>

            <div class="col-md-2 my-2">
                <div class="input-group mb-2">
                    <input type="text"
                           class="form-control"
                           id="size"
                           name="size"
                           placeholder="Size">
                </div>
            </div>

            <div class="col-12 my-2">
                <div class="form-group">
                    <textarea class="form-control"
                              id="description"
                              rows="5"
                              name="description"
                              placeholder="Description of Product" required> </textarea>
                </div>
            </div>

            <div class="col-12 my-2">
                <div class="form-group">
                    <label for="image">Product Image</label>
                    <input type="file" class="form-control-file" id="image" name="image" required>
                </div>
            </div>

            <div class="col-lg-2 col-md-4 my-2">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="sale" value="sale" name="sale">
                    <label class="form-check-label" for="sale">On Sale?</label>
                </div>
            </div>

            <div class="col-lg-10 col-md-8 my-2">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text">£</div>
                    </div>
                    <input type="text"
                           class="form-control"
                           id="sale_price"
                           name="sale_price"
                           placeholder="Sale Price">
                </div>
            </div>

            <div class="col my-2">
                <label for="marketplace_link">Marketplace Link</label>
                <input type="text"
                       class="form-control"
                       id="marketplace_link"
                       name="marketplace_link"
                       placeholder="Paste your Facebook Marketplace link here...">
            </div>

        </div>
    </form>

@endsection
