@extends('layouts.app')
@section('content')

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            {{ session('success') }}
        </div>
    @endif

    <!-- Add new modal trigger -->
    <button type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#exampleModal">
        Add new image
    </button>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col" class="text-center">Image</th>
        </tr>
        </thead>
        <tbody>
        @foreach($images as $image)
            <tr>
                <td class="text-primary">
                    <div type="button" class="pointer" data-toggle="modal" data-id="{{ $image->id }}"
                         data-title={{ $image->name }}"
                         data-target="#editModal_{{ $image->id }}">
                        {{ strlen($image->name) > 20 ? substr($image->name,0,20)."..." : $image->name }}
                    </div>
                </td>
                <td class="text-center">
                    <img width="100"
                         src="public/images/gallery/{{ $image->image }}"
                         alt="">
                </td>
            </tr>

            <!-- Edit Image -->
            <div class="modal fade" id="editModal_{{ $image->id }}" tabindex="-1" aria-labelledby="editImageLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="/gallery/{{ $image->id }}/update" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit | {{ strlen($image->name) > 20 ? substr($image->name,0,20)."..." : $image->name }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="col-12 my-2">
                                    <input type="text"
                                           class="form-control"
                                           id="name"
                                           name="name"
                                           value="{{ $image->name }}"
                                           placeholder="Title of Image" required>
                                </div>

                                <div class="col-12 my-2">
                                    <div class="custom-file">
                                        <input type="file"
                                               class="custom-file-input"
                                               id="image"
                                               name="image"
                                               value="{{ $image->image }}">
                                        <label class="custom-file-label"
                                               for="image">{{ $image->image ? strlen($image->image) > 20 ? substr($image->image,0,20)."..." : $image->image : 'Browse Image' }}</label>
                                        <small class="form-text text-muted">
                                            Image
                                        </small>
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <a href="/gallery/delete/{{ $image->id }}" class="btn btn-danger del">Delete</a>
                                <button type="submit" class="btn btn-success">Update Image</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        @endforeach
        </tbody>
    </table>

    <!-- Add New Image -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <form action="/gallery/store" method="post" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add new Image</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="col-12 my-2">
                            <input type="text"
                                   class="form-control"
                                   id="name"
                                   name="name"
                                   placeholder="Title of Image" required>
                        </div>

                        <div class="col-12 my-2">
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" class="form-control-file" id="image" name="image" required>
                            </div>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Save Image</button>
                    </div>

                </form>

            </div>
        </div>
    </div>

@endsection
