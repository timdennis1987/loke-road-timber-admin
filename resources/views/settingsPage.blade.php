@extends('layouts.app')
@section('content')

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ session('success') }}
        </div>
    @endif

    <form action="/settings/update" method="post" enctype="multipart/form-data">
        @csrf

        <button type="submit" class="btn btn-success">Update Settings</button>

        <ul class="nav nav-tabs my-3" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link active" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                   aria-controls="contact" aria-selected="true"><i class="fas fa-id-card-alt"></i></a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="address-tab" data-toggle="tab" href="#address" role="tab"
                   aria-controls="address" aria-selected="false"><i class="fas fa-home"></i></a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="hours-tab" data-toggle="tab" href="#hours" role="tab" aria-controls="hours"
                   aria-selected="false"><i class="fas fa-clock"></i></a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="social-tab" data-toggle="tab" href="#social" role="tab" aria-controls="social"
                   aria-selected="false"><i class="fas fa-hashtag"></i></a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" id="about-tab" data-toggle="tab" href="#about" role="tab" aria-controls="about"
                   aria-selected="false"><i class="fas fa-address-card"></i></a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <div class="col">
                    <h2>Contact</h2>
                </div>

                @foreach($contactInfo as $contact)
                    <div class="col my-2">
                        <input type="text"
                               class="form-control"
                               name="{{ $contact->value }}"
                               value="{{ $contact->description }}"
                               placeholder="{{ $contact->name }}">
                    </div>
                @endforeach

            </div>
            <div class="tab-pane fade" id="address" role="tabpanel" aria-labelledby="address-tab">
                <div class="col">
                    <h2>Address</h2>
                </div>

                @foreach($addressInfo as $address)
                    <div class="col my-2">
                        <input type="text"
                               class="form-control"
                               name="{{ $address->value }}"
                               value="{{ $address->description }}"
                               placeholder="{{ $address->description }}">
                    </div>
                @endforeach

            </div>
            <div class="tab-pane fade" id="hours" role="tabpanel" aria-labelledby="hours-tab">
                <div class="col">
                    <h2>Hours</h2>
                </div>

                @foreach($hours as $day)

                    <div class="col">
                        <div class="form-group row">
                            <label for="{{ $day->value }}" class="col-sm-2 col-form-label">{{ $day->name }}</label>
                            <div class="col-sm-10">
                                <input type="text"
                                       class="form-control"
                                       id="{{ $day->value }}"
                                       name="{{ $day->value }}"
                                       value="{{ $day->description }}"
                                       placeholder="{{ $day->description }}">
                            </div>
                        </div>
                    </div>

                @endforeach

            </div>
            <div class="tab-pane fade" id="social" role="tabpanel" aria-labelledby="social-tab">
                <div class="col">
                    <h2>Social</h2>
                </div>

                @foreach($socials as $social)

                    <div class="col my-2">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text" style="color:{{ $social->class }};">{!! $social->icon !!}</div>
                            </div>
                            <input type="text"
                                   class="form-control"
                                   name="{{ $social->value }}"
                                   value="{{ $social->description }}"
                                   placeholder="{{ $social->name }}">
                        </div>
                    </div>

                @endforeach

            </div>
            <div class="tab-pane fade" id="about" role="tabpanel" aria-labelledby="about-tab">

                <div class="col">
                    <h2>Home Page</h2>
                </div>

                <div class="col-12 my-2">
                    <div class="custom-file">
                        <input type="file"
                               class="custom-file-input"
                               id="home_image"
                               name="home_image"
                               value="{{ $home->image }}">
                        <label class="custom-file-label"
                               for="home_image">{{ $home->image ? strlen($home->image) > 20 ? substr($home->image,0,20)."..." : $home->image : 'Browse Image' }}</label>
                        <small class="form-text text-muted">
                            Image for home page
                        </small>
                    </div>
                </div>

                <div class="col my-2">
                    <input type="text"
                           class="form-control"
                           name="home_h1"
                           value="{{ $home->h1 }}">
                </div>

                <div class="col my-2">
                    <input type="text"
                           class="form-control"
                           name="home_h2"
                           value="{{ $home->h2 }}">
                </div>

                <hr>

                <div class="col">
                    <h2>About Page</h2>
                </div>

                <div class="col-12 my-2">
                    <div class="custom-file">
                        <input type="file"
                               class="custom-file-input"
                               id="image"
                               name="image"
                               value="{{ $about->image }}">
                        <label class="custom-file-label"
                               for="image">{{ $about->image ? strlen($about->image) > 20 ? substr($about->image,0,20)."..." : $about->image : 'Browse Image' }}</label>
                        <small class="form-text text-muted">
                            Image
                        </small>
                    </div>
                </div>

                <div class="col-12 my-2">
                    <div class="form-group">
                    <textarea class="form-control"
                              id="description"
                              rows="5"
                              name="description"
                              placeholder="Description of Product">{{ $about->description }}</textarea>
                    </div>
                    </div>
                </div>

            </div>
        </div>

    </form>

@endsection
