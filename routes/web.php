<?php

use App\Http\Controllers\GalleryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SettingController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/register', function () {
    return redirect('/login');
});

//Home / Dashboard
Route::get('', [HomeController::class, 'index'])->name('home');

//Products / Categories
Route::get('timber', [ProductController::class, 'timberList'])->name('timber');
Route::get('timber/edit/{id}', [ProductController::class, 'timberEdit']);

Route::get('sheds', [ProductController::class, 'shedList'])->name('sheds');
Route::get('sheds/edit/{id}', [ProductController::class, 'shedEdit']);

Route::get('household', [ProductController::class, 'householdList'])->name('household');
Route::get('household/edit/{id}', [ProductController::class, 'householdEdit']);

Route::get('garden', [ProductController::class, 'gardenList'])->name('garden');
Route::get('garden/edit/{id}', [ProductController::class, 'gardenEdit']);

Route::get('product/new', [ProductController::class, 'viewNewProductPage']);
Route::post('product/new/store', [ProductController::class, 'storeNewProduct']);
Route::post('product/{id}/update', [ProductController::class, 'updateProduct']);
Route::get('product/delete/{id}', [ProductController::class, 'deleteProduct']);

//Gallery
Route::get('gallery', [GalleryController::class, 'galleryPage']);
Route::post('gallery/store', [GalleryController::class, 'storeNewImage']);
Route::post('gallery/{id}/update', [GalleryController::class, 'updateImage']);
Route::get('gallery/delete/{id}', [GalleryController::class, 'deleteImage']);

//Settings
Route::get('settings', [SettingController::class, 'settingsPage']);
Route::post('settings/update', [SettingController::class, 'updateSettings']);
